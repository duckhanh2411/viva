﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VivaMVC5.Startup))]
namespace VivaMVC5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
