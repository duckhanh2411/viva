﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using VivaMVC5.Models;

namespace VivaMVC5.Helper
{
    public class ProductDBContext : IdentityDbContext<Users>
    {
        public DbSet<Customers> Customers { get; set; }
        public DbSet<WorkCustomers> WorkCustomers { get; set; }
        public DbSet<WorkCustomerDetails> WorkCustomerDetails { get; set; }
        public DbSet<Finances> Finances { get; set; }
        public DbSet<Products> Products { get; set; }
        public DbSet<WorkSuppliers> WorkSuppliers { get; set; }
        //public DbSet<AspNetUsers> AspNetUsers { get; set; }
        //public DbSet<AspNetRoles> AspNetRoles { get; set; }
        //public DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
    }
}
