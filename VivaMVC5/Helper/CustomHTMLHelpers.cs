﻿
namespace VivaMVC5.Helper
{
    public static class CustomHTMLHelpers
    {
        public static string RemoveHTMLTags(string HTMLCode)
        {
            return System.Text.RegularExpressions.Regex.Replace(HTMLCode, "<[^>]*>", "");
        }
    }
}