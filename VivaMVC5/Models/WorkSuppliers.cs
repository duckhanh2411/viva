using System;

namespace VivaMVC5.Models
{
    public partial class WorkSuppliers
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public decimal? Water { get; set; }
        public decimal? Bottle { get; set; }
        public DateTime? CreatedDate { get; set; }
        public virtual Products Product { get; set; }
    }
}
