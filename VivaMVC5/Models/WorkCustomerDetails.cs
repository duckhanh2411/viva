namespace VivaMVC5.Models
{
    public partial class WorkCustomerDetails
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public decimal? Water { get; set; }
        public decimal? Bottle { get; set; }
        public int? WorkCustomersId { get; set; }
        public Products Product { get; set; }
        public virtual WorkCustomers WorkCustomers { get; set; }
    }
}
