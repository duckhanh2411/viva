using System;
using System.Collections.Generic;

namespace VivaMVC5.Models
{
    public partial class WorkCustomers
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public decimal? Money { get; set; }
        public DateTime? CreatedDate { get; set; }
        public virtual Customers Customer { get; set; }
        //public virtual ICollection<WorkCustomerDetails> WorkCustomerDetails { get; set; }
    }
}
