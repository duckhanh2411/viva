using System.Collections.Generic;

namespace VivaMVC5.Models
{
    public partial class Products
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public string Flavor { get; set; }
        public decimal? RealVolume { get; set; }
        public string Image { get; set; }
        public decimal? WaterBottle { get; set; }
        public decimal? Bottle { get; set; }
        public string HtmlDesciption { get; set; }
        public string PackedType { get; set; }
        //public virtual ICollection<WorkSupplier> WorkSuppliers{ get; set;}
        //public ICollection<WorkCustomerDetail> WorkCustomerDetails { get; set; }
    }
}
