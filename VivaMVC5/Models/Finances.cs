using System;

namespace VivaMVC5.Models
{
    public partial class Finances
    {
        public int Id { get; set; }
        public string LevyName { get; set; }
        public decimal? LevyFree { get; set; }
        public string PayName { get; set; }
        public decimal? PayFree { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
