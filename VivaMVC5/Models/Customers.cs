﻿using System.Collections.Generic;
using System.ComponentModel;

namespace VivaMVC5.Models
{
    public partial class Customers
    {
        [DisplayName("Mã khách hàng")]
        public int Id { get; set; }
        [DisplayName("Tên khách hàng")]
        public string Name { get; set; }
        [DisplayName("Địa chỉ")]
        public string Address { get; set; }
        [DisplayName("Điện thoại")]
        public decimal? Phone { get; set; }     
    }
}
