using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VivaMVC5.Helper;
using VivaMVC5.Models;

namespace NuocTinhKhietViva.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ImportController : Controller
    {
        private ProductDBContext _context = new ProductDBContext();

        public ActionResult Index()
        {
            ViewBag.Products = _context.Products;
            return View();
        }

        [HttpPost]
        public ActionResult WorkSupplier()
        {
            var model = new WorkSuppliers();
            model.ProductId = int.Parse(Request.Form["productId"]);
            model.Water = int.Parse(Request.Form["countWater"]);
            model.Bottle = int.Parse(Request.Form["countBottle"]);
            model.CreatedDate = DateTime.UtcNow;

            _context.WorkSuppliers.Add(model);

            //TODO: Refactoring in next version
            var product = new Products();
            product.Bottle += model.Bottle;
            product.WaterBottle += model.Water;

            _context.Entry(product).State = EntityState.Modified;
            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult GetWorkSuppliers([DataSourceRequest]DataSourceRequest request)
        {
            var workSuppliers = _context.WorkSuppliers.Include(s => s.Product);
            return Json(workSuppliers.ToDataSourceResult(request));
        }
    }
}
