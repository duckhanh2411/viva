﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VivaMVC5.Helper;

namespace VivaMVC5.Controllers
{
    public class HomeController : Controller
    {
        private ProductDBContext _context = new ProductDBContext();

        public ActionResult Index()
        {
            return View(_context.Products);
        }

        public ActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public ActionResult Detail(int productId)
        {
            return View(_context.Products.FirstOrDefault(p => p.Id == productId));
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}