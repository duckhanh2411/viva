﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VivaMVC5.Helper;
using VivaMVC5.Models;

namespace VivaMVC5.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ExportController : Controller
    {
        private ProductDBContext _context = new ProductDBContext();
        // GET: Export
        public ActionResult Index()
        {
            ViewBag.Products = _context.Products;
            ViewBag.Customers = _context.Customers;
            return View();
        }


        [HttpPost]
        public ActionResult WorkCustomer()
        {
            var workCustomer = new WorkCustomers();
            workCustomer.CustomerId = int.Parse(Request.Form["customerId"]);
            workCustomer.Money = int.Parse(Request.Form["totalMoney"]);
            workCustomer.CreatedDate = DateTime.UtcNow;

            _context.WorkCustomers.Add(workCustomer);
            _context.SaveChanges();

            List<int> water = new List<int>();
            List<int> bottle = new List<int>();
            water.AddRange(new int[] { int.Parse(Request.Form["waterVictory"]), int.Parse(Request.Form["waterViva"]), int.Parse(Request.Form["waterBirico"]) });
            bottle.AddRange(new int[] { int.Parse(Request.Form["bottleVictory"]), int.Parse(Request.Form["bottleViva"]), int.Parse(Request.Form["bottleBirico"]) });


            for (int i = 1; i <= 3; i++)
            {
                var workCustomerDetail = new WorkCustomerDetails();
                workCustomerDetail.ProductId = i;
                workCustomerDetail.Water = water[i - 1];
                workCustomerDetail.Bottle = bottle[i - 1];
                workCustomerDetail.WorkCustomersId = workCustomer.Id;

                _context.WorkCustomerDetails.Add(workCustomerDetail);

                //TODO: Refactoring in next version
                var product = new Products();
                product.Bottle += workCustomerDetail.Bottle;
                product.WaterBottle += workCustomerDetail.Water;

                _context.Entry(product).State = EntityState.Modified;
                _context.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult GetWorkCustomers([DataSourceRequest]DataSourceRequest request)
        {
            var workCustomers = _context.WorkCustomers.Include(c => c.Customer);
            return Json(workCustomers.ToDataSourceResult(request));
        }

        public ActionResult GetWorkCustomerDetails(int workCustomerId, [DataSourceRequest] DataSourceRequest request)
        {
            var workCustomerDetails = _context.WorkCustomerDetails.Where(cd => cd.WorkCustomersId == workCustomerId).ToDataSourceResult(request);
            return Json(workCustomerDetails);
        }
    }
}