﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VivaMVC5.Helper;
using VivaMVC5.Models;

namespace VivaMVC5.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class FinanceController : Controller
    {
        private ProductDBContext _context = new ProductDBContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            var check = _context.Finances.ToDataSourceResult(request);
            return Json(check);
        }

        [AcceptVerbs("Post")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Finances> finances)
        {
            if (finances != null && ModelState.IsValid)
            {
                foreach (var finance in finances)
                {
                    _context.Finances.Attach(finance);
                    _context.Finances.Add(finance);
                    _context.SaveChanges();
                }
            }

            return Json(finances.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Finances> finances)
        {
            if (finances != null && ModelState.IsValid)
            {
                foreach (var finance in finances)
                {
                    _context.Entry(finance).State = EntityState.Modified;
                    _context.SaveChanges();
                }
            }

            return Json(finances.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Finances> finances)
        {
            if (finances.Any())
            {
                foreach (var finance in finances)
                {
                    _context.Finances.Attach(finance);
                    _context.Finances.Remove(finance);
                    _context.SaveChanges();
                }
            }

            return Json(finances.ToDataSourceResult(request, ModelState));
        }
    }
}
