﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VivaMVC5.Helper;
using VivaMVC5.Models;

namespace VivaMVC5.Controllers
{
    public class CustomerController : Controller
    {
        private ProductDBContext _context = new ProductDBContext();

        // GET: Customers
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            return Json(_context.Customers.ToDataSourceResult(request));
        }

        [AcceptVerbs("Post")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Customers> customers)
        {
            if (customers != null && ModelState.IsValid)
            {
                foreach (var customer in customers)
                {
                    _context.Customers.Attach(customer);
                    _context.Customers.Add(customer);
                    _context.SaveChanges();
                }
            }

            return Json(customers.ToDataSourceResult(request));
        }

        [AcceptVerbs("Post")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Customers> customers)
        {
            if (customers != null && ModelState.IsValid)
            {
                foreach (var customer in customers)
                {
                    _context.Entry(customer).State = EntityState.Modified;
                    _context.SaveChanges();
                }
            }

            return Json(customers.ToDataSourceResult(request));
        }

        [AcceptVerbs("Post")]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Customers> customers)
        {
            if (customers.Any())
            {
                foreach (var customer in customers)
                {
                    _context.Customers.Attach(customer);
                    _context.Customers.Remove(customer);
                    _context.SaveChanges();
                }
            }

            return Json(customers.ToDataSourceResult(request));
        }
    }
}
